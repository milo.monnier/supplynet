R package supplynet is dedicated to the assessment of robustness and efficiency
metrics of supply networks, including their spatial dimension at multiple scales.